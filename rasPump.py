from sys import exit
from os import remove
import argparse
from json import load
from collections import OrderedDict
from time import sleep
from datetime import datetime

try:
    from discord import File as discordFile
    from discord.ext import commands, tasks
except ImportError:
    print('ERROR: Cannot import \'discord\'!')
    exit(-1)

try:
    from gpiozero import LED
except ImportError:
    print('ERROR: Cannot import \'gpiozero\'!')
    exit(-1)

try:
    import picamera
except ImportError:
    print('ERROR: Cannot import \'picamera\'!')
    exit(-1)

try:
    from board import SCL, SDA
except ImportError:
    print('ERROR: Cannot import \'board\'!')
    exit(-1)

try:
    import busio
except ImportError:
    print('ERROR: Cannot import \'busio\'!')
    exit(-1)

try:
    from adafruit_seesaw.seesaw import Seesaw
except ImportError:
    print('ERROR: Cannot import \'adafruit_seesaw\'!')
    exit(-1)


#
# Utilities
#
def read_file_to_json_dict(file_path, order=None):
    try:
        with open(file_path, 'r') as f_id:
            if order is not None and order.lower() == 'ordered':
                json_dict = load(f_id, object_pairs_hook=OrderedDict)
            else:
                json_dict = load(f_id)
    except IOError:
        print('ERROR: cannot open file \'{file_path}\''.format(file_path=file_path))
        json_dict = dict()
    except ValueError:
        print('ERROR: cannot decode file \'{file_path}\''.format(file_path=file_path))
        json_dict = dict()

    return json_dict


def validate_parameter(a_dict, param_name):
    is_param_valid = True
    if param_name not in a_dict:
        print('ERROR: cannot find \'{param_name}\' in config file!'.format(param_name=param_name))
        is_param_valid = False

    return is_param_valid


def validate_config_parameters(config_dict):
    is_config_valid = True

    if not validate_parameter(config_dict, 'discord'):
        is_config_valid = False
    else:
        if not validate_parameter(config_dict['discord'], 'token'):
            is_config_valid = False

    if not validate_parameter(config_dict, 'pump'):
        is_config_valid = False
    else:
        if not validate_parameter(config_dict['pump'], 'pin'):
            is_config_valid = False

    if not validate_parameter(config_dict, 'stemma'):
        is_config_valid = False
    else:
        if not validate_parameter(config_dict['stemma'], 'id'):
            is_config_valid = False

    return is_config_valid


def get_timestamp():
    return datetime.now().strftime('%Y%m%d%A_%H%M%S')


#
# RasPump Class
#
class RasPumpClass:
    def __init__(self, pump_dict, stemma_dict, camera_dict):
        self._pump_dict = pump_dict
        self._stemma_dict = stemma_dict
        self._camera_dict = camera_dict

        # Initialize
        self.__pump_device = self.__pump_initialize()
        self.__stemma_device = self.__stemma_initialize()
        self.__camera_device = self.__camera_initialize()
        # Log data
        self.__log_file_name = 'log.txt'

    #
    # Pump
    #
    def __pump_initialize(self):
        # Create 'LED' pump
        return LED(self._pump_dict['pin'])

    def pump(self, sleep_time):
        # Turn on pump
        self.__pump_device.on()
        # Sleep
        sleep(sleep_time)
        # Turn off pump
        self.__pump_device.off()

    #
    # Stemma Moisture Device
    #
    def __stemma_initialize(self):
        # Start moisture device
        i2c_bus = busio.I2C(SCL, SDA)
        return Seesaw(i2c_bus, addr=int(self._stemma_dict['id'], 16))

    def __get_moisture(self):
        return self.__stemma_device.moisture_read()

    def __get_temperature(self):
        return self.__stemma_device.get_temp()

    #
    # Camera
    #
    @staticmethod
    def __camera_initialize():
        # Start camera
        camera = picamera.PiCamera()
        # Turn off LED
        camera.led = False

        return camera

    def take_photo(self):
        timestamp = get_timestamp()
        photo_path = '{timestamp}.png'.format(timestamp=timestamp)
        self.__camera_device.capture(photo_path)
        return photo_path

    #
    # Log
    #
    def log_stemma_data(self):
        # Get moisture measurement
        moisture = self.__get_moisture()
        # Get temperature measurement
        temperature = self.__get_temperature()
        # Get timestamp
        timestamp = datetime.now().strftime('%Y,%m,%d,%H,%M,%S')

        # Append moisture and temperature to timestamp
        log_data = '{timestamp},{moisture},{temperature:.2f}\n'.format(timestamp=timestamp,
                                                                       moisture=moisture,
                                                                       temperature=temperature)

        # Append data to log file
        with open(self.__log_file_name, 'a') as f_id:
            f_id.write(log_data)

    def read_stemma_log_data(self):
        # Read log file
        with open(self.__log_file_name, 'r') as f_id:
            # Get last line, strip whitespace, and split
            last_log_line = f_id.readlines()[-1].strip().split(',')

        # Split line with comma to check if eight values are present
        if len(last_log_line) == 8:
            # Extract moisture and temperature
            moisture = last_log_line[-2]
            temperature = last_log_line[-1]
        else:
            moisture = -1
            temperature = -1

        return moisture, temperature


#
# Discord Bot + Cog
#
class RasPumpCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

        self.__log_stemma_data.start()

    @commands.Cog.listener()
    async def on_ready(self):
        print('Logged on as {bot_name}'.format(bot_name=self.bot.user))

    @commands.Cog.listener()
    async def on_message(self, ctx):
        author = ctx.author.name
        timestamp = ctx.created_at.strftime('%Y%m%d%A_%H%M%S')

        if author != self.bot.user.name:
            print('[{timestamp}] Author: {author}\nMessage: \'{msg}\''.format(timestamp=timestamp, author=author, msg=ctx.content))

    @tasks.loop(seconds=10.0)
    async def __log_stemma_data(self):
        RasPump.log_stemma_data()

    @commands.command(name='hello')
    async def on_hello(self, ctx):
        await ctx.send('Hello!')

    @commands.command(name='pump')
    async def on_pump(self, ctx):
        message_split = ctx.message.content.split()
        if len(message_split) != 2:
            msg = 'Syntax should be \'$pump <time_in_seconds>\'!'
            await ctx.send(msg)
        else:
            try:
                time_in_seconds = float(message_split[1])
            except ValueError:
                await ctx.send('Hmm... I cannot recognize this time format: \'{}\''
                               .format(message_split[1]))
            else:
                await ctx.send('Started pumping...')
                RasPump.pump(time_in_seconds)
                await ctx.send('Done pumping!')

    @commands.command(name='photo')
    async def on_photo(self, ctx):
        # Notify
        await ctx.send('Taking photo...')
        # Take photo and get photo path
        photo_path = RasPump.take_photo()
        # Send photo
        await ctx.send('Here it is!')
        await ctx.send(file=discordFile(photo_path))
        # Remove photo
        remove(photo_path)

    @commands.command(name='moisture')
    async def on_moisture(self, ctx):
        # Get moisture measurement
        moisture, temperature = RasPump.read_stemma_log_data()
        if moisture == -1:
            msg = 'There was an error reading Stemma data... Sorry...'
        else:
            msg = 'Moisture: {moisture}'.format(moisture=moisture)
        # Send moisture measurement
        await ctx.send(msg)

    @commands.command(name='temperature')
    async def on_temperature(self, ctx):
        # Get moisture measurement
        moisture, temperature = RasPump.read_stemma_log_data()
        if moisture == -1:
            msg = 'There was an error reading Stemma data... Sorry...'
        else:
            msg = 'Temperature: {temperature}'.format(temperature=temperature)
        # Send moisture measurement
        await ctx.send(msg)


def setup(bot):
    bot.add_cog(RasPumpCog(bot))


if __name__ == '__main__':
    # Create parser
    parser = argparse.ArgumentParser(description='Ras(berry Pi) Pump.')
    parser.add_argument('config_json', help='Path to configuration JSON', type=str)
    # Get parser results
    args = parser.parse_args()

    # Read file to JSON dictionary
    config_dict = read_file_to_json_dict(args.config_json)
    if not config_dict:
        exit(-1)

    # Validate parameters
    if not validate_config_parameters(config_dict):
        exit(-1)
    # Get parameters
    discord_dict = config_dict['discord']
    pump_dict = config_dict['pump']
    stemma_dict = config_dict['stemma']
    # TODO: Add camera dictionary
    camera_dict = None

    # Start RasPump
    RasPump = RasPumpClass(pump_dict, stemma_dict, camera_dict)

    # Start RasPumpBot
    RasPumpBot = commands.Bot(command_prefix='!')
    setup(RasPumpBot)
    RasPumpBot.run(discord_dict['token'])
