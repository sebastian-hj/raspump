# RasPump: Ras(pberry Pi) Pump

## Description

Discord bot to care of plants.


## Hardware

| Item | Tested with |
| :---: | :---: |
| Raspberry Pi | Raspberry Pi Zero W |
| Raspberry Pi Camera Module | Raspberry Pi Camera Module v1.3 NOIR |
| Peristaltic Pump | [Peristaltic Liquid Pump with Silicone Tubing](https://www.adafruit.com/product/1150) |
| IRLB8721PBF N-Channel MOSFET | [Infineon IRLB8721PBF](https://www.mouser.com/ProductDetail/infineon/irlb8721pbf/) |
| Moisure Sensor | [Adafruit STEMMA Soil Sensor](https://www.adafruit.com/product/4026) |

### Circuit Diagram

To be added...


## Software

### Python Version

Tested on Python 3.7.3 and 3.7.4

#### Package/Module Requirements
| Package | Comments |
| :---: | :---: |
| [gpiozero](https://gpiozero.readthedocs.io/) | Raspberry Pi GPIO management |
| [picamera](https://picamera.readthedocs.io/) | Raspberry Pi Camera control |
| [adafruit-circuitpython-seesaw](https://github.com/adafruit/Adafruit_CircuitPython_seesaw) | ``board``, ``busio``, ``Seesaw`` modules |
| [discord](https://discordpy.readthedocs.io/en/latest/) | Discord API |

### Configuration File

A single JSON configuration file must be used to configure the different hardware and/or modules.

#### Pump

```
    "pump": {
        "pin": 
    }
```

- ``pump.pin``: GPIO pin in Raspberry Pi for pump

#### Moisture Sensor

```
    "stemma": {
        "id": 
    }
```

- ``stemma.id``: I2C ID for moisture device (in hex)

#### Discord Bot

```
    "discord": {
        "token": ""
    }
```

- ``discord.token``: Discord bot token

## Usages

### Run

```
python3 rasPump.py config.json
```

### Discord Commands
| Command | Description |
| :---: | :---: |
| ``!hello`` | Say hello | 
| ``!photo`` | Take photo |
| ``!pump <time_in_secods>`` | Pump for ``<time_in_secods>`` |
| ``!moisture`` | Get moisture measurement |
| ``!temperature`` | Get temperature measurement (in Celsius) |

## References

- [Adafruit STEMMA Soil Sensor - I2C Capacitive Moisture Sensor](https://learn.adafruit.com/adafruit-stemma-soil-sensor-i2c-capacitive-moisture-sensor/python-circuitpython-test)
- [Automated Indoor Gardener](https://www.hackster.io/hackershack/automated-indoor-gardener-a90907)